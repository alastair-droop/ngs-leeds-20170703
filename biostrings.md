# Introduction to Biostrings

## Setup

### Install the tools we need in R

We will be using `Biostrings`, which is part of the `Bioconductor` system:

~~~R
source("https://bioconductor.org/biocLite.R")
biocLite(c("Biostrings", "Homo.sapiens"))
library(Biostrings)
library(Homo.sapiens)
~~~

### Get the data to play with:

The data (and code) are located in a git repository in Bitbucket. Clone this by:

~~~bash
git clone https://alastair-droop@bitbucket.org/alastair-droop/ngs-leeds-20170703.git
cd ngs-leeds-20170703
~~~


## Sequences in R

First, we'll play with a basic sequence object:

~~~R
seq <- DNAString('AAGAGGAGCGGAGCGGCTTTTAGTTCAAAACTGACATTCAGCCTCCTGATTGGCGGATAGAGCAATGAGA')
length(seq)
subseq(seq, start=10, width=10)
alphabetFrequency(seq)
alphabetFrequency(seq)[DNA_BASES]
~~~

We can also read sequences from `FASTA` formatted-files (**NB:** there is no need to decompress the files, if they are `.gz` compressed):

~~~R
rRNA <- readDNAStringSet(file.path('data', 'rRNA.fasta.gz'))
length(rRNA)
width(rRNA)
~~~

Load a large `FASTA` file:

~~~R
chr9 <- readDNAStringSet(file.path('data', 'chr9.fasta.gz'))
length(chr9)
width(chr9)
chr9 <- chr9[[1]]
~~~

This file contains the GRCh38 version of chromosome 9.  We also have another file, which is a list of the genes on chromosome 9, and their positions:

~~~R
gene.pos <- read.table(file.path('data', 'gene-pos.txt'), header=TRUE, sep='\t', row.names=1, colClasses=c('character', 'numeric', 'numeric'))
genes <- Views(chr9, start=gene.pos$start, end=gene.pos$end)
~~~

Now that we have loaded this data, we can analyse it. For example, we can calculate the GC content of these genes, and compare to the GC content of the whole chromosome:

~~~R
gc.genes <- letterFrequency(genes, 'GC')[,1] / width(genes)
gc.chr <- letterFrequency(chr9, 'GC') / length(chr9)
hist(gc.genes)
abline(v=gc.chr, col='red4')
~~~

~~~R
window.size <- 1e5
v <- Views(chr9, start=seq(from=1, to=length(chr9), by=window.size), width=window.size)
gcc <- letterFrequency(v, 'GC')[,1] / width(v)
plot(gcc, xlab='chr9 Position', ylab='GC Content', type='l')
~~~

## Searching in Sequences:

We can easily look for small patterns within a large sequence. For example, we can look for the first 25 nucleotides of a gene of interest:

~~~R
search.seq <- DNAString('GAGTGAATGAATGAAAATGATTTTA')
res <- matchPattern(search.seq, chr9)
res <- matchPattern(search.seq, chr9, max.mismatch=1)

gene.res <- gene.pos[gene.pos$start <= i & gene.pos$end >= i,]
gene.ids <- rownames(gene.res)
select(Homo.sapiens, keytype='ENSEMBL', keys=gene.ids, columns=c('ENSEMBL', 'SYMBOL', 'ENTREZID'))
~~~
